$(function() {
    $("[data-toggle='tooltip']".tooltip())
    $("[data-toggle='popover']".popover())

    $('.carrousel').carrousel({
        interval: 1000
    });
    $('#contact').on('show.bs.modal', function (params) {
        console.log('modal is being shown');
        $('#contact-btn').removeClass('btn-outline-info');
        $('#contact-btn').addClass('btn-outline-info');
        $('#contact-btn').prop('dissabled', true);
    });
    $('#contact').on('shown.bs.modal', function (params) {
        console.log('modal had been shown');
    });
    $('#contact').on('hdie.bs.modal', function (params) {
        console.log('modal is being hidden');
    });
    $('#contact').on('hidden.bs.modal', function (params) {
        console.log('modal has been hidden');
        $('#contact-btn').prop('dissabled', false);
    });
});